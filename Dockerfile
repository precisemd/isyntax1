FROM	ubuntu:xenial	AS	XENIAL

RUN	apt-get update && apt-get install -y libboost-thread1.58 libboost-filesystem1.58 libboost-system1.58 libboost-serialization1.58

FROM	ubuntu:bionic

RUN	apt-get update && apt-get install -y libx11-6 libgl1 python3-pillow python3-numpy 

COPY	--from=XENIAL /usr/lib/x86_64-linux-gnu/libboost_*.so.1.58.0 /usr/local/lib/

WORKDIR	/opt/iSyntax

ADD	iSyntax/	./

ADD	https://www.openpathology.philips.com/wp-content/uploads/AnimalSlides/1.isyntax ./

ENV	NVIDIA_DRIVER_CAPABILITIES graphics
ENV	LD_LIBRARY_PATH /usr/local/lib

CMD	[ "python3", "example_isyntax.py" ]
