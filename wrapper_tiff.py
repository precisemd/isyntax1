from iSyntax.wrapper import iSyntaxSlide
import rasterio
import time

if "__main__"  == __name__:
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument('input', nargs='?', default='1.isyntax')
    p.add_argument('outputFile', nargs='?', default='1_1.tif')
    p.add_argument('--tile_width', type=int, default=512)
    p.add_argument('--tile_height', type=int, default=512)
    p.add_argument("--level", type=int, default=0)
    p.add_argument('--batch_size', type=int, default=2)
    p.add_argument('--num_workers', type=int, default=1)

    args = p.parse_args()
    
    slide = iSyntaxSlide(args.input)

    slide_width, slide_height = slide.level_dimensions[0]


    writer = rasterio.open(args.outputFile, 'w', driver='GTiff', 
                           width=slide_width, height=slide_height, count=3, dtype='uint8',
                           tfw=True, tiled=True, profile='baseline',
                           blockxsize=args.tile_width, blockysize=args.tile_height,
                           compress="JPEG", jpegtablesmode=3, jpeg_quality=92, photometric='YCBCR')

    t0 = time.time()

    for r0 in range(0, slide_height, args.tile_height):
        r1 = r0 + args.tile_height
        r1 = min(slide_height, r1)
        for c0 in range(0, slide_width, args.tile_width):
            c1 = c0 + args.tile_width
            c1 = min(slide_width, c1)

            img = slide.read_region((c0, r0), 0, (c1-c0, r1-r0))
            img = img[:,:,:3].transpose((2,0,1))

            writer.write(img, indexes=(1,2,3),
                         window=((r0,r1), (c0,c1)) )

    print("Conversion took %.3f seconds" % (time.time() - t0))

    print("Building overviews...")
    t0 = time.time()
    writer.build_overviews((2,4,8,16,32,64))
    writer.close()
    print("Overviews took %.3f seconds" % (time.time() - t0))
