﻿#(C) Koninklijke Philips Electronics N.V. 2015

#All rights are reserved. Reproduction or transmission in whole or in part, in
#any form or by any means, electronic, mechanical or otherwise, is prohibited
#without the prior written permission of the copyright owner.

from _isyntax import *
from ctypes import *
import sys
from PIL import Image

import numpy as np

targetDir = '.'
fileName = '1.isyntax'

print("Reading", fileName)
print("Target output", targetDir)

iSyntaxImage = iSyntax(fileName, targetDir)

print("iSyntax ver", iSyntaxImage.Version())
print("iSyntax dir", dir(iSyntaxImage))

print("Levels")
for level in range(0, iSyntaxImage.NumberOfImages()):
    dimension = iSyntaxImage.GetImageSize(level)
    print(level, dimension.X, dimension.Y, dimension.Width, dimension.Height)

print('Barcode =', iSyntaxImage.Barcode())

macro = iSyntaxImage.Macro()
text_file = open(targetDir + '/macro.jpg', 'wb')
text_file.write(macro)
text_file.close()

label = iSyntaxImage.Label()
text_file = open(targetDir + '/label.jpg', 'wb')
text_file.write(label)
text_file.close()

#for level in range(0, iSyntaxImage.NumberOfImages()):
for level in range(0, 3):
    regions = iSyntaxImage.GetRegions(level)
    for region in range(0, len(regions)):
        rect = regions[region]
        print("Rect class", rect.__class__)
        print('level', level, 'region', region, ': X.Y => width:height :', (rect.get('x'), rect.get('y'), rect.get('width'), rect.get('height')))

print('Scan Offsets:',iSyntaxImage.ScanOffset())

for level in range(0, iSyntaxImage.NumberOfImages()):
    print('Color Format at level', level, iSyntaxImage.GetImageColorFormat(level))

iSyntaxImage.SetMaskColor([254, 254, 254])

level = iSyntaxImage.NumberOfImages() - 2
width = iSyntaxImage.GetImageSize(level).Width
height = iSyntaxImage.GetImageSize(level).Height
size = width * height * 3
bmpImageBuf = (c_ubyte * size)()

processingSettings = iSyntax.ProcessingSettings()
processingSettings.EnableColorCorrection = False
processingSettings.EnableCLAHE = False
processingSettings.EnableSharpening = False
processingSettings.setGamma(1.0)
iSyntaxImage.Processing(processingSettings)

iSyntaxImage.GetRegion(level, 0, 0, width, height, byref(bmpImageBuf))

bmpImage = Image.frombuffer('RGB', (width, height), bmpImageBuf, 'raw', 'RGB', 0 , 1)
bmpImage.save(targetDir + '/region.png')

del iSyntaxImage
