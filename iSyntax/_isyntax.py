﻿#(C) Koninklijke Philips Electronics N.V. 2015

#All rights are reserved. Reproduction or transmission in whole or in part, in
#any form or by any means, electronic, mechanical or otherwise, is prohibited
#without the prior written permission of the copyright owner.

from ctypes import *
import os
import platform
import inspect

class iSyntax:

    # Class variables and functions
    __lib = ''

    if(platform.system() == 'Linux'):
        __lib = CDLL(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) + '/libisyntax.so')
    elif(platform.system() == 'Windows'):
        __lib = WinDLL(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) + '\\isyntax.dll')
    else:
        raise Exception('%s platform is not supported'%(platform.system()))

    __lastErrorMessage = ''

    # call-back function for Error Handling
    __errorHandlingFuncWrapper = CFUNCTYPE(None, c_wchar_p)
    def __errorHandlingFunc(message):
        iSyntax.__lastErrorMessage = message

    __lib.ISYNTAX_SetErrorHandler(__errorHandlingFuncWrapper(__errorHandlingFunc))

    @staticmethod
    def __GetLastError():
        return iSyntax.__lastErrorMessage

    ColorFormat = {0:'RGB24', 1: 'YCbCr32'}

    # Constructor
    def __init__(self, fileName, cacheFolder):
        self.__imageObject = c_void_p()
        filename = c_wchar_p(fileName)
        cachefolder = c_wchar_p(cacheFolder)
        if(iSyntax.__lib.ISYNTAX_OpenFile(filename, cachefolder, byref(self.__imageObject)) == 0):
            raise Exception(iSyntax.__GetLastError())

    # Explicit Destructor
    def __del__(self):
        if(iSyntax.__lib.ISYNTAX_CloseFile(self.__imageObject) == 0):
            raise Exception(iSyntax.__GetLastError())

    # Version of iSyntax SDK.
    @staticmethod
    def Version():
        version = c_wchar_p('')
        if(iSyntax.__lib.ISYNTAX_GetVersion(byref(version)) == 0):
            raise Exception(iSyntax.__GetLastError())
        return version.value

    # Number of images or levels available in the whole-slide image pyramid.
    def NumberOfImages(self):
        result = c_int(0)
        if(iSyntax.__lib.ISYNTAX_GetNumberOfImages(self.__imageObject, byref(result)) == 0):
            raise Exception(iSyntax.__GetLastError())
        return result.value

    # Barcode value.
    def Barcode(self):
        result = []
        buf = c_void_p()
        numBarcodes = c_int(0)
        bufSizes = c_void_p()
        if(iSyntax.__lib.ISYNTAX_GetBarcodes(self.__imageObject, byref(buf), byref(numBarcodes), byref(bufSizes)) == 0):
            raise Exception(iSyntax.__GetLastError())
        if(numBarcodes.value > 0):
            sizes = (c_int*numBarcodes.value).from_address(bufSizes.value)
            buffers = (c_int32*numBarcodes.value).from_address(buf.value)
            result = string_at(buffers[0],sizes[0])
        return result

    # Macro image as a JPEG encoded string.
    def Macro(self):
        result = ''
        buf = c_void_p()
        bufSizes = c_int(0)
        if(iSyntax.__lib.ISYNTAX_GetMacroImage(self.__imageObject, byref(buf), byref(bufSizes)) == 0):
            raise Exception(iSyntax.__GetLastError())
        if(bufSizes.value > 0):
            result = string_at(buf, bufSizes.value)
        return result

    # Label image as a JPEG encoded string.
    def Label(self):
        result = ''
        buf = c_void_p()
        bufSizes = c_int(0)
        if(iSyntax.__lib.ISYNTAX_GetLabelImage(self.__imageObject, byref(buf), byref(bufSizes)) == 0):
            raise Exception(iSyntax.__GetLastError())
        if(bufSizes.value > 0):
            result = string_at(buf, bufSizes.value)
        return result

    class Rect(Structure):
        _fields_ = [
            ("X", c_int),
            ("Y", c_int),
            ("Width", c_int),
            ("Height", c_int)]

    # Returns dimensions of the specified image in {x, y, width, height} dictionary.
    def GetImageSize(self, level):
        imageSize = iSyntax.Rect();
        if(iSyntax.__lib.ISYNTAX_GetImageSize(self.__imageObject, level, byref(imageSize)) == 0):
            raise Exception(iSyntax.__GetLastError())
        return imageSize;

    # Returns scanned regions of the specified image in list of {x, y, width, height} dictionary
    def GetRegions(self, level):
        buf = pointer(c_int(0))
        regionSizes = c_int(0)
        if(iSyntax.__lib.ISYNTAX_GetRegions(self.__imageObject, level, byref(buf), byref(regionSizes)) == 0):
            raise Exception(iSyntax.__GetLastError())
        result = []
        for region in range(0, regionSizes.value):
            result.append({'x':buf[4*region + 0], 'y':buf[4*region + 1], 'width':buf[4*region + 2], 'height':buf[4*region + 3]})
        return result

    # Returns physical dimensions of a pixel of the specified image (in millimeters).
    def GetImagePixelSize(self, level):
        width = c_double(0)
        height = c_double(0)
        if(iSyntax.__lib.ISYNTAX_GetImagePixelSize(self.__imageObject, level, byref(width), byref(height)) == 0):
            raise Exception(iSyntax.__GetLastError())
        return {'width':width.value, 'height':height.value}

    # Returns the physical offset of the scanned area on the glass slide (in millimeters).
    def ScanOffset(self):
        offsetX = c_double(0)
        offsetY = c_double(0)
        if(iSyntax.__lib.ISYNTAX_GetScanOffset(self.__imageObject, byref(offsetX), byref(offsetY)) == 0):
            raise Exception(iSyntax.__GetLastError())
        return (offsetX.value, offsetY.value)

    # Returns color format of the specified image.
    def GetImageColorFormat(self, level):
        colorFormat = c_int(0)
        if(iSyntax.__lib.ISYNTAX_GetImageColorFormat(self.__imageObject, level, byref(colorFormat)) == 0):
            raise Exception(iSyntax.__GetLastError())
        return iSyntax.ColorFormat[colorFormat.value]

    # Returns the specified pixel region.
    # bufferType:
    # 0-> memory buffer allocated by user
    # 1-> texture object allocated by user through SDK
    def GetRegion(self, level, x, y, width, height, buffer, bufferType = 0):
        if(iSyntax.__lib.ISYNTAX_GetImageRegion(self.__imageObject, level, x, y, width, height, buffer, bufferType) == 0):
            raise Exception(iSyntax.__GetLastError())

    # Set RGB color used for sparse (i.e. non-scanned) regions.
    def SetMaskColor(self, colorRGB):
        color = (c_ubyte*len(colorRGB))()
        for i in range(len(colorRGB)):
            color[i] = colorRGB[0]
        if(iSyntax.__lib.ISYNTAX_SetMaskColor(self.__imageObject, color) == 0):
            raise Exception(iSyntax.__GetLastError())

    # Sets the processing settings through processing object.
    def Processing(self, processingSettings):
        processObject = c_void_p()
        if (iSyntax.__lib.ISYNTAX_CreateProcessingObject(byref(processObject)) == 0):
            raise Exception(iSyntax.__GetLastError())
        try:
            if (iSyntax.__lib.ISYNTAX_EnableCLAHE(processObject, processingSettings.EnableCLAHE and c_bool(True) or c_bool(False)) == 0):
                raise Exception(iSyntax.__GetLastError())
            if (iSyntax.__lib.ISYNTAX_SetClipLimit(processObject, processingSettings.ClaheClipLimit) == 0):
                raise Exception(iSyntax.__GetLastError())
            if (iSyntax.__lib.ISYNTAX_EnableSharpening(processObject, processingSettings.EnableSharpening and c_bool(True) or c_bool(False)) == 0):
                raise Exception(iSyntax.__GetLastError())
            if (iSyntax.__lib.ISYNTAX_SetGain(processObject, processingSettings.SharpeningGain) == 0):
                raise Exception(iSyntax.__GetLastError())
            if (iSyntax.__lib.ISYNTAX_EnableColorCorrection(processObject, processingSettings.EnableColorCorrection and c_bool(True) or c_bool(False)) == 0):
                raise Exception(iSyntax.__GetLastError())
            if (iSyntax.__lib.ISYNTAX_SetBlackOffset(processObject, processingSettings.BlackOffset) == 0):
                raise Exception(iSyntax.__GetLastError())
            if (iSyntax.__lib.ISYNTAX_SetWhiteOffset(processObject, processingSettings.WhiteOffset) == 0):
                raise Exception(iSyntax.__GetLastError())
            if (iSyntax.__lib.ISYNTAX_SetGamma(processObject, processingSettings.Gamma) == 0):
                raise Exception(iSyntax.__GetLastError())
            if (iSyntax.__lib.ISYNTAX_SetProcessing(self.__imageObject, processObject) == 0):
                raise Exception(iSyntax.__GetLastError())
        finally:
            iSyntax.__lib.ISYNTAX_ReleaseProcessingObject(processObject)

    class ProcessingSettings:
        def __init__(self):
            self.SharpeningGain = c_float(2.0)
            self.ClaheClipLimit = c_float(1.2)
            self.BlackOffset = c_float(0/4095.0)
            self.WhiteOffset = c_float(3500/4095.0)
            self.Gamma = c_float(2.4)
            self.EnableColorCorrection = False
            self.EnableCLAHE = False
            self.EnableSharpening = False

        def setSharpeningGain(self, value):
            self.SharpeningGain = c_float(value)

        def setClaheClipLimit(self, value):
            self.ClaheClipLimit = c_float(value)

        def setBlackOffset(self, value):
            self.BlackOffset = c_float(value/4095.0)

        def setWhiteOffset(self, value):
            self.WhiteOffset = c_float(value/4095.0)

        def setGamma(self, value):
            self.Gamma = c_float(value)

    # Gets the maximum allowed texture size by GPU.
    @staticmethod
    def GetMaxTextureSize():
        width = c_int(0)
        height = c_int(0)
        if(iSyntax.__lib.ISYNTAX_GetMaximumTextureSize(byref(width), byref(height)) == 0):
            raise Exception(iSyntax.__GetLastError())
        return {'width':width.value, 'height':height.value}

    # Gets the dynamic library handle
    @staticmethod
    def Library():
        return iSyntax.__lib

    # Makes the context of SDK current
    def MakeContextCurrent(self):
        if(iSyntax.__lib.ISYNTAX_MakeContextCurrent() == 0):
            raise Exception(iSyntax.__GetLastError())

    # Texture Object
    class TextureObject:
        # Constructor
        def __init__(self, width, height):
            self.__textureObject = c_void_p()
            if(iSyntax.Library().ISYNTAX_CreateTextureTargetObject(byref(self.__textureObject), width, height) == 0):
                raise Exception(iSyntax.__GetLastError())

        # Destructor
        def __del__(self):
            if(iSyntax.Library().ISYNTAX_ReleaseTextureTargetObject(self.__textureObject) == 0):
                raise Exception(iSyntax.__GetLastError())

        # Gets texture object pointer
        def GetObject(self):
            return self.__textureObject

        # Gets the frame ID of the texture object
        def GetTextureID(self):
            textureID = c_int(0)
            if(iSyntax.Library().ISYNTAX_GetTextureID(self.__textureObject, byref(textureID)) == 0):
                raise Exception(iSyntax.__GetLastError())
            return textureID.value