from pixelengine import PixelEngine
from softwarerendercontext import SoftwareRenderContext
from softwarerenderbackend import SoftwareRenderBackend

import rasterio
import time
import numpy as np

def calculate_tiff_dimensions(view, start_level):
    """
    Set the TIFF tile size
    Note that TIFF mandates tile size in multiples of 16
    Calculate the Image Dimension range from the View at the Start Level
    :param view: Source View
    :param start_level: Starting Level
    :return: tiff_dim_x, tiff_dim_y
    """
    x_start = view.dimension_ranges(start_level)[0][0]
    x_step = view.dimension_ranges(start_level)[0][1]
    x_end = view.dimension_ranges(start_level)[0][2]
    y_start = view.dimension_ranges(start_level)[1][0]
    y_step = view.dimension_ranges(start_level)[1][1]
    y_end = view.dimension_ranges(start_level)[1][2]
    range_x = x_end - x_start + x_step
    range_y = y_end - y_start + y_step

    # As the multi-resolution image pyramid in TIFF
    #  shall follow a down sample factor of 2
    # Normalize the Image Dimension from the coarsest level
    #  so that a downscale factor of 2 is maintained across levels
    # Size Normalization
    tiff_dim_x = range_x
    tiff_dim_y = range_y
    return tiff_dim_x, tiff_dim_y


def round_up(value, multiple):
    """
    Round Up
    :param value: Value
    :param multiple: Mu,tiple
    :return: Rounded up value
    """
    result = value
    if (value % multiple) > 0:
        result = value + multiple - (value % multiple)
    return result

if "__main__"  == __name__:
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument('input', nargs='?', default='1.isyntax')
    p.add_argument('outputFile', nargs='?', default='1_2.tif')
    p.add_argument('--tile_width', type=int, default=512)
    p.add_argument('--tile_height', type=int, default=512)
    p.add_argument("--level", type=int, default=0)
    p.add_argument('--batch_size', type=int, default=2)
    p.add_argument('--num_workers', type=int, default=1)

    args = p.parse_args()
     
    render_backend = SoftwareRenderBackend()
    render_context = SoftwareRenderContext()

    pixel_engine = PixelEngine(render_backend, render_context)
    slide = pixel_engine['in']
    slide.open(args.input)

    view = slide['WSI'].source_view
    data_envelopes = view.data_envelopes(0)

    start_level = 0

    slide_width, slide_height = calculate_tiff_dimensions(view, 0)

    writer = rasterio.open(args.outputFile, 'w', driver='GTiff', 
                           width=slide_width, height=slide_height, count=3, dtype='uint8',
                           tfw=True, tiled=True, profile='baseline',
                           blockxsize=args.tile_width, blockysize=args.tile_height,
                           compress="JPEG", jpegtablesmode=3, jpeg_quality=92, photometric='YCBCR')

    t0 = time.time()

    for r0 in range(0, slide_height, args.tile_height):
        r1 = r0 + args.tile_height
        r1 = min(slide_height, r1)
        for c0 in range(0, slide_width, args.tile_width):
            c1 = c0 + args.tile_width
            c1 = min(slide_width, c1)
            patch = (c0+1, c1, r0+1, r1, 0)
            regions_req = view.request_regions([patch], data_envelopes, False,
                                                   [255, 255, 255], pixel_engine.BufferType(0))


            regions_ready = pixel_engine.wait_any(regions_req)

            region = regions_ready[0]

            #img = np.empty((r1-r0)*(c1-c0)*3, dtype='uint8')
            img = np.empty(shape=(r1-r0, c1-c0,3), dtype='uint8')
            region.get(img)

            img = img[:,:,:3].transpose((2,0,1))

            writer.write(img, indexes=(1,2,3),
                         window=((r0,r1), (c0,c1)) )

    print("Conversion took %.3f seconds" % (time.time() - t0))

    print("Building overviews...")
    t0 = time.time()
    writer.build_overviews((2,4,8,16,32,64))
    writer.close()
    print("Overviews took %.3f seconds" % (time.time() - t0))
